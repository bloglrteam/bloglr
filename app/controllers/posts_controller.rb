class PostsController < ApplicationController
    before_action :set_post, only: [:show, :edit, :update, :destroy]
    before_action :authenticate, except: [:index, :show]
    
    # GET /posts
    # GET /posts.json
    def index
		order = params[:order]
		@posts = Post.all
		@posts.sort_by! { |p| [p.total_rating, p.created_at] }.reverse! if order == "rated"
        @posts.each{|post|
            post[:content].gsub!(/#([\w][\w\-]{1,18}[\w]?)\b/i){
                tag = Regexp.last_match[1]
                "<a href=\"/tag/#{tag.downcase}\"><mark>##{tag}</mark></a>"
            }
        }
    end


    # GET /posts/1
    # GET /posts/1.json
    def show
		@post[:content].gsub!(/#([\w][\w\-]{1,18}[\w]?)\b/i){
            tag = Regexp.last_match[1]
            "<a href=\"/tag/#{tag}\"><mark>##{tag}</mark></a>"
        }
        @post[:content].gsub!(/(?:\n\r?|\r\n?)/, '<br/>')
    end

    # GET /posts/new
    def new
        @post = Post.new
    end

    # GET /posts/1/edit
    def edit
    end

    # POST /posts
    # POST /posts.json
    def create
        @post = Post.new(post_params)
        @post.user_id = current_user.id
        respond_to do |format|
            if @post.save
                format.html { redirect_to @post, notice: 'Post was successfully created.' }
                format.json { render action: 'show', status: :created, location: @post }
            else
                format.html { render action: 'new' }
                format.json { render json: @post.errors, status: :unprocessable_entity }
            end
        end
    end

    # PATCH/PUT /posts/1
    # PATCH/PUT /posts/1.json
    def update
        respond_to do |format|
            if @post.update(post_params)
                format.html { redirect_to @post, notice: 'Post was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: 'edit' }
                format.json { render json: @post.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /posts/1
    # DELETE /posts/1.json
    def destroy
        @post.destroy
        respond_to do |format|
            format.html { redirect_to posts_url }
            format.json { head :no_content }
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
        @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
        params.require(:post).permit(:title, :content, :created_at)
    end
end
