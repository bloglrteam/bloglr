class TagsController < ApplicationController
    before_action :set_tag, only: [:show]
    
    # GET /tags
    # GET /tags.json
    # We want to output the data in coherent manor so that we can then generate
    # a nice graph.
    def index
        @tags = Tag.all.sort_by{|tag| tag.count}.reverse
    end

    # GET /tags/tag
    # GET /tags/tag.json
    def show
        @tag.posts.each{|post|
            post[:content].gsub!(/#([\w][\w\-]{1,18}[\w]?)\b/i){
                tag = Regexp.last_match[1]
                "<a href=\"/tag/#{tag.downcase}\"><mark>##{tag}</mark></a>"
            }
        }
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
        @tag = Tag.where(name: params[:name]).first
    end

    # Never trust parameters from the scary internet, only allow the white list
    # through.
    def tag_params
        params.require(:tag).permit(:name)
    end
end
