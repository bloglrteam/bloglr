class CommentsController < ApplicationController
  before_action :login_check, only: [:create]
  def create

  	@comment = Comment.new(comment_params[:comment])
    @post = Post.find(comment_params[:post_id])
    @comment.post_id = @post.id
    @comment.user_id = current_user.id
  	if @comment.save
  		redirect_to @comment.post, notice: "Comment successfully created"
  	else
  		redirect_to :back, error: "Error creating comment"
    end

  end

  def update
  end

  def destroy
  	@comment = Comment.find(comment_params[:id])
  	if @comment.destroy
  		redirect_to @comment.post, notice: "Comment successfully deleted"
  	else
  		redirect_to :back, error: "Error deleting comment"
  	end
  end


private

  def login_check
    unless signed_in?
      session[:return_to] = URI(request.referer).path
      redirect_to signin_path, notice: 'Please sign in to access this page.'
    end
  end

  def comment_params
  	params.permit!()
  end

end
