class RatingsController < ApplicationController
  before_action :authenticate
  before_action :check_user, only: [:create, :update]

  def create
    @rating = Rating.new(rate_params[:rating])
    @rating.post_id = @post.id
    @rating.user_id = current_user.id
    if @rating.save
      respond_to do |format|
        format.html { redirect_to post_path(@post), :notice => "Your rating has been saved" }
        format.json { @post }
      end
    end
  end

  def update
    @rating = current_user.ratings.find_by_post_id(@post.id)
    if @rating.update_attributes(rate_params[:rating])
      respond_to do |format|
        format.html { redirect_to post_path(@post), :notice => "Your rating has been updated" }
        format.json { @post}
      end
    end
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def check_user
    @post = Post.find(rate_params[:post_id])
    if current_user.id == @post.user_id
      redirect_to post_path(@post), :alert => "You cannot rate for your own post"
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
  def rate_params
    params.permit(:post_id, rating: [:value])
  end

end
