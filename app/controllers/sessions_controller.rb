class SessionsController < ApplicationController
  def new
    @title = 'Sign in'
  end

  def create
    user = User.authenticate(session_params[:session][:username],
                              session_params[:session][:password])
    if user.nil?
      flash.now[:error] = 'Invalid username/password combination'
      @title = 'Sign in'
      render 'new'
    else
      sign_in user
      flash[:notice] = "Welcome back, #{user.username}!"
      redirect_back_or posts_path
    end
  end

  def destroy
    flash[:notice] = "Adios #{current_user.username}!"
    sign_out
    redirect_to root_path
  end


  private

  def session_params()
    params.permit!
  end
end
