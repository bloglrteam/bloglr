json.array!(@posts) do |post|
  json.extract! post, :id, :title, :content, :total_rating, :comment_count
  json.created_at print_post_time(post)
  json.user post.user.username
  json.show_url post_path(post)
  json.user_url user_path(post.user)
  json.url post_url(post, format: :json)
end

