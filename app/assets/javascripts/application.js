//= require_self

/**
 * This is a specific function that'll retrieve the values from table rows
 * excluding the `thead' element. There is a optional parameter that allows
 * one to limit the number of elements retrieved.
 *
 * After this is executed, the original table is removed.
 *
 * NOTE: This is designed specifically for tables that only have two entries
 *       per row. Otherwise data will be left out.
 * 
 * @param {String} name of ID of the table
 * @param {Integer} limits the number of results to `limit + 1` where the
 *                  extra element is the `other' set which includes the
 *                  complete value of all items limited by. [OPTIONAL]
 * @return {Dictionary} containing two arrays (`name' and `value') and two
 *                      integers (`length' and `count')
 */
function get_table_data(table, limit)
{
    var obj = {
        name: [],
        value: [],
        length: 0,
        count: 0
    }
    var mTable = document.getElementById(table);
    var rowLength = mTable.rows.length;
    var whatRows = (isNaN(limit) || limit > rowLength)
                    ? rowLength
                    : limit + 1;

    for(var i = 1; i < whatRows; i++)
    {
        var mCell = mTable.rows.item(i).cells;
        obj.name[obj.name.length] = mCell[0].textContent;
        obj.value[obj.value.length] = parseInt(mCell[1].textContent);
        obj.count += obj.value[obj.value.length - 1];
        obj.length += 1;
    }
    
    // This is a little dirty, but its the best for the moment
    if(whatRows < rowLength)
    {
        var p = obj.value.length;
        obj.name[obj.name.length] = "other";
        obj.value[p] = 0;
        for(var i = whatRows; i < rowLength; i++)
        {
            var mCell = mTable.rows.item(i).cells;
            obj.value[p] += parseInt(mCell[1].textContent);
        }
        obj.length += 1;
        obj.count += obj.value[p];
    }
    
    // Annndddd...... it's gone!
    mTable.remove();

    return obj;
}

// Notice Fading In and Out Functions
function fadeout(element) {
		var op = 1;  // initial opacity
		var timer = setInterval(function () {
		    if (op <= 0.1){
		        clearInterval(timer);
		        element.style.display = 'none';
		    }
		    element.style.opacity = op;
		    element.style.filter = 'alpha(opacity=' + op * 100 + ")";
		    op -= op * 0.1;
		}, 50);
	}

function fadein(element) {
		var op = 0.1;  // initial opacity
		var timer = setInterval(function () {
		    if (op >= 0.9){
		        clearInterval(timer);
		    }
		    element.style.opacity = op;
		    element.style.filter = 'alpha(opacity=' + op * 100 + ')';
		    op += op * 0.1;
		}, 50);
	}

function fade()
{
	if (document.getElementById("notice"))
	{
		fadein(document.getElementById("notice"));	

		setTimeout(function(){
			fadeout(document.getElementById("notice"));
		},3000);
	}

}

window.onload = fade;


/**
 * Based upon an input string, generate a value between 0 and 360.
 * This function will simple add up the ASCII integer code values and
 * modulus it by 360.
 *
 * NOTE: This function should be using in conjunction with a color space
 *       based upon HSL/HSV. Though it could be using for any `circular'
 *       calculation.
 *
 * @param {String} any amount or collection of symbols
 * @return {Integer} value within a range of 0..360
 */
function text_to_hue(string)
{
    var total = 0;
    for(var i = 0; i < string.length; i++)
    {
        total += string.charCodeAt(i);
    }
    return ((total * string.length * 1.62) % 360).toFixed();
}

/**
 * This is an OnReady alternative, similar to JQuery's $.ready() function
 */
var onReady = function(callback) {
    if (document.readyState === "complete")
        callback();
    else if (document.addEventListener)
        document.addEventListener('DOMContentLoaded', callback);
    else if (document.attachEvent)
        document.attachEvent('onreadystatechange', callback);
};
