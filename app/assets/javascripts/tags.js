// JavaScript for Tags Controller
//= require application

/**
 * This object will be used to store and retrieve some settings
 */
var config = {
    other: "other",
    colors: []
};

/**
 * Create a pie chart based upon data provided by the table on /tags.
 *
 * @param {Object} this is the dataset
 * @see application.js::get_table_data()
 */
function create_pie_chart(data)
{
    var lastend = 0;
    var canvas = document.getElementById("tags_pie");
    var center = {
        y: Math.floor(canvas.height/2),
        x: Math.floor(canvas.width/2)
    };
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for(var i = 0; i < data.length; i++)
    {
        if(data.name[i] == config.other)
        {
            config.colors[config.colors.length] = "hsl(0, 0%, 45%)";
            ctx.fillStyle = config.colors[config.colors.length - 1];
        } else {
            config.colors[config.colors.length] = "hsl(" + text_to_hue(data.name[i]) + ", 80%, 45%)";
            ctx.fillStyle = config.colors[config.colors.length - 1];
        }
        ctx.beginPath();
        ctx.moveTo(center.x, center.y);

        // arc(x-center, y-center, radius, arc-start, arc-end)
        ctx.arc(center.x, center.y, center.y, lastend,
            lastend + (Math.PI * 2 * (data.value[i]/data.count)));
        
        ctx.lineTo(center.x, center.y);
        ctx.fill();

        // Move the arc-start further along
        lastend += (Math.PI * 2 * (data.value[i]/data.count));
    }
}

/**
 * Creates a table with tags, counts, and colours
 *
 * @param {Object} this is the dataset
 * @see application.js::get_table_data()
 */
function create_data_table(data)
{
    var tbody = document.getElementById("tags_data_table").getElementsByTagName("tbody")[0];

    for(var i = 0; i < data.length; i++)
    {
        var row = tbody.insertRow(i);
        var cell0 = row.insertCell(0);
        if(data.name[i] === "other"){
            cell0.textContent = data.name[i];
        } else {
            cell0.innerHTML = "<a href=\"tag/" + data.name[i] + "\">#" + data.name[i] + "</a>";
        }
        var cell1 = row.insertCell(1);
        cell1.textContent = data.value[i];
        var cell2 = row.insertCell(2);
        cell2.style.backgroundColor = config.colors[i];
    }
}

function run() { 
    // I think 9 is a nice number, this will return top 9 used tags
    // and extra element for the total of other...
    var data = get_table_data("tags_table", 9);
    create_pie_chart(data);
    create_data_table(data);
}

/**
 * This is a nice alternative to JQuery's $.ready() method
 */
onReady(run);
