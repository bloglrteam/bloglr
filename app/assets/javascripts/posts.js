//= require application

var refreshPosts, updateRating;

if (!String.linkify) {
  String.prototype.linkify = function() {
    var tagParse;
    tagParse = /\B(#\w[\w\-]{1,18}\w)\b/gim;
    return this.replace(tagParse, "<a href=\"#\">#$1</a>");
  };
}

refreshPosts = function() {
  var xmlhttp, color;
	
  var selected = document.getElementById('order_rated').checked;
  
  var action = "posts.json";
  if (selected){ action += "?order=rated";}
  xmlhttp = void 0;
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    var articles, html, newarticle, ni, x;
    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
	
	  if (selected){var rated = "checked=''"; var time = '';}
	  else {var rated = ""; var time = 'checked=""';}
      html = '<form accept-charset="UTF-8" action="/posts" id="order_form" method="get"> <b>Time:</b> <input id="order_time" name="order" type="radio" onchange="refreshPosts()" '+ time +' value="time" />  <b>Rated:</b> <input id="order_rated" name="order" type="radio" onchange="refreshPosts()" '+ rated + ' value="rated" /> </form>';
      articles = JSON.parse(xmlhttp.responseText);
      x = 0;
      while (x < articles.length) {
		if(articles[x]["total_rating"] > 0) color = "green";
		else if(articles[x]["total_rating"] < 0) color = "red";
		else color = "neutral";

        html += "<article class='post'><header>";
		html += "<span class='rating-circle-"+ color +" right'>";
		html += articles[x]["total_rating"] +"</span><h3>";
        html += articles[x]["title"];
        html += "</h3></header><p>";
        html += articles[x]["content"];
        html += "</p><footer>";

		if(articles[x]['comment_count'] > 0){
			html += "<div class='left_comment'>(<a href='/posts/"
			html += articles[x]['id'];
			html += "#comments'>"+articles[x]['comment_count']+"</a>)</div>"
		}		
		html +="<div class='actions'><span>   ";
        html += articles[x]["created_at"];
        html += "</span><span> by <a href='" + articles[x]["user_url"] +"' >";
        html += articles[x]["user"];
        html += "</a></span></div>";
		html +="<div class='actions'><span><small><a href='";
        html += articles[x]["show_url"];
        html += "'>Read more...</a> ";
        html += "</small></span></div></footer></article>";
        x++;
      }
      ni = document.getElementById("middle");
      newarticle = document.createElement("article");
      newarticle.innerHTML = html;
      ni.innerHTML = "";
      ni.appendChild(newarticle);
	  console.log("UPDATED");
    }
  };

  xmlhttp.open("GET", action , true);
  xmlhttp.send();
};


window.onload=function(){

fade();

if (window.location.pathname == "/posts" && document.getElementById('order_form')) {	 
  setInterval(refreshPosts, 5000);
}
};



