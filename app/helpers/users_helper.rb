module UsersHelper
	def possessive(word)
    if word.end_with?('s')
      word + "'"
    else
     word + "'s"
    end
  end
end
