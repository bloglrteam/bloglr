module PostsHelper
  def rating_ballot
    if @rating = current_user.ratings.find_by_post_id(params[:id])
      @rating
    else
      current_user.ratings.new
    end
  end

  def current_user_rating
    if @rating = current_user.ratings.find_by_post_id(params[:id])
      @rating.value
    else
      "N/A"
    end
  end

  def print_post_time(post)
    distance_of_time_in_words((Time.now.utc - post.created_at).round) + " ago"
  end

  def rating_display(post)
    rating = post.total_rating
    if rating > 0
      "<span id='rating-value' class='rating-circle-green right'>#{rating}</span>"
    elsif rating < 0
      "<span id='rating-value' class='rating-circle-red right'>#{rating}</span>"
    else
      "<span id='rating-value' class='rating-circle-neutral right'>#{rating}</span>"
    end
  end

  def rating_between_arrows(post)
    rating = post.total_rating
    if rating > 0
      "<span id='rating-value1' class='rating-circle-green move'>#{rating}</span>"
    elsif rating < 0
      "<span id='rating-value1' class='rating-circle-red move'>#{rating}</span>"
    else
      "<span id='rating-value1' class='rating-circle-neutral move'>#{rating}</span>"
    end
  end

end
