class Tag < ActiveRecord::Base
	has_and_belongs_to_many :posts, join_table: :posts_tags

    validates :name,
        presence: true,
        format: { with: /[\w\-]{1,20}/i, message: "Tag must be of form #TAG and less then 20 characters long"},
        uniqueness: {case_sensitive: true}

    def count
        self.posts.count
    end
end
