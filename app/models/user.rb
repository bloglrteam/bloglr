class User < ActiveRecord::Base
	has_many :posts
	has_many :comments
  has_many :ratings
  has_many :rated_posts, through: :ratings, source: :posts

  validates :username, presence: true, length: {maximum: 20}, uniqueness: {case_sensitive: true}
  validates :password, length: {within: 6..40}
  validates_confirmation_of :password
  before_save :encrypt_password

  def has_password?(submitted_password)
    password == encrypt(submitted_password)
  end

  def self.authenticate(username, submitted_password)
    user = find_by_username(username)
    return nil if user.nil?
    return user if user.has_password?(submitted_password)
  end

  def self.authenticate_with_salt(id, cookie_salt)
    user = find_by_id(id)
    (user && user.salt == cookie_salt) ? user : nil
  end

  private

  def encrypt_password
    self.salt = make_salt if new_record?
    self.password = encrypt(password)
  end

  def encrypt(string)
    secure_hash("#{salt}--#{string}")
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end
end
