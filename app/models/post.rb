class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :ratings
  has_many :raters, through: :ratings, source: :users
  has_and_belongs_to_many :tags, join_table: :posts_tags

  validates :title, presence: true
  validates :content, presence: true
  validates :user_id, presence: true
  after_save :parse_tags

  default_scope :order => 'posts.created_at DESC'

  # We parse the tags from the content and then replace them
  # with link versions, after which we add new tags to the db
  def parse_tags
      tags = find_tags
      tags.each do |tag|
        @tag = Tag.find_or_create_by(name: tag)
        @tag.posts << self
      end
  end

  def total_rating
      self.ratings.sum(:value)
  end

  # find tags within content
  def find_tags
      tags = []
      self[:content].scan(/#([\w][\w\-]{1,18}[\w]?)\b/i){
        tag = Regexp.last_match[1]
        tags << tag.downcase
      }
      tags
  end

  def comment_count
    self.comments.count
  end
end
